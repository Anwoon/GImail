<?php

namespace App\Repository;

use App\Entity\Email;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Email|null find($id, $lockMode = null, $lockVersion = null)
 * @method Email|null findOneBy(array $criteria, array $orderBy = null)
 * @method Email[]    findAll()
 * @method Email[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Email::class);
    }

    public function findByCategory($id)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT e.*, c.name FROM email as e
            INNER JOIN category as c
            ON e.category_id = c.id
            WHERE c.id = :id
            ';
        $request = $conn->prepare($sql);
        $request->execute(['id' => $id]);


        return $request->fetchAll();

    }

    public function findBySearch($label,$filter)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT e.* FROM email as e
            WHERE e.".$filter."
            LIKE '%".$label."%'
            ";


        $request = $conn->prepare($sql);
        $request->execute();

        return $request->fetchAll();
    }
}
