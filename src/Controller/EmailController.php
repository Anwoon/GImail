<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Contact;
use App\Entity\Email;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\FileUploader;

class EmailController extends Controller
{
    /**
     * @Route("/email/new", name="email_new")
     */
    public function new(Request $request, FileUploader $fileUploader)
    {
        $email = new Email();
        $email->setDate(new \DateTime());
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $array_category = [];

        foreach($categories   as $category) {
            $array_category[$category->getName()] = $category;
        }

        $form = $this->createFormBuilder($email)
            ->add('sender', EmailType::class,['label' => 'Créateur'])
            ->add('recipient', EmailType::class,['label' => 'Destinataire'])
            ->add('subject', TextType::class, ['label' => 'Sujet' ])
            ->add('category', ChoiceType::class, ['label' => '', 'choices' => $array_category])
            ->add('message', TextareaType::class, ['label' => 'Message'])
            ->add('file', FileType::class, array('label' => 'Image (JPG ou PNG)', 'attr' => ['class' => '']))
            ->add('submit', SubmitType::class, ['label' => 'Envoyer', 'attr' => ['class' => 'waves-effect waves-light btn']])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $contact = $this->getDoctrine()->getRepository(Contact::class)->findOneBy(['mail' => $email->getRecipient()]);

            if ($contact == NULL) {
                return new Response("L'adresse que vous avez validée ne fait pas parti de votre agenda de contact, veuillez lier un contact à cette addresse mail pour l'utiliser");
            } else {

                if ($email->getRecipient() === $contact->getMail()) {

                    /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                    $file = $form['file']->getData();

                    $fileName = $fileUploader->upload($file);

                    $email->setFile($fileName);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($email);
                    $em->flush();
                } else {
                    return new Response("L'adresse que vous avez validée ne fait pas parti de votre agenda de contact, veuillez lier un contact à cette addresse mail pour l'utiliser");
                }
                return $this->redirect('/email/home');
            }
        }
        return $this->render('email/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/email/home", name="email_home")
     */
    public function home(Request $request)
    {
        $mail = $this->getDoctrine()->getRepository(Email::class)->findAll();

        $category = $this->getDoctrine()->getRepository(Category::class)->findAll();

        $mount = count($mail);

        $defaultData = array('message' => 'Type your message here');

        $search_bar = $this->searchBarForm($defaultData);

        $search_bar->handleRequest($request);

        if ($search_bar->isSubmitted() && $search_bar->isValid()) {

            $label = htmlspecialchars($search_bar->getData()['search']);
            $filter = htmlspecialchars($search_bar->getData()['trier']);
            $search_value_string = 'Searching for "'. $label .'"';

            $mails = $this->getDoctrine()
                ->getRepository(Email::class)
                ->findBySearch($label,$filter);

            return $this->render('search/index.html.twig', [
                'mails' => $mails,
                'category' => $search_value_string,
            ]);

        }


        return $this->render('email/home.html.twig', [
            'mails' => $mail,
            'mount' => $mount,
            'categorys' => $category,
            'searchBar' => $search_bar->createView()
        ]);
    }

    /**
     * @Route("/email/{id}", name="email_index")
     */
    public function index($id)
    {
        $mail = $this->getDoctrine()->getRepository(Email::class)->findByCategory($id);

        $mount = count($mail);

        $category = $this->getDoctrine()->getRepository(Category::class)->find($id);

        return $this->render('email/index.html.twig', [
            'mails' => $mail,
            'mount' => $mount,
            'category' => $category
        ]);
    }

    /**
     * @Route("/email/show/{id}", name="email_show")
     */
    public function show($id)
    {
        $mail = $this->getDoctrine()->getRepository(Email::class)->find($id);

        return $this->render('email/show.html.twig', [
            'mail' => $mail,
        ]);
    }

    public function searchBarForm($defaultData){

        return $this->createFormBuilder($defaultData)
            ->add('search', TextType::class)
            ->add('trier', ChoiceType::class, array(
                'label' => '',
                'choices'  => array(
                    'Par sujet' => 'subject',
                    'Par Message' => 'message'
                )
            ))
            ->add('save', SubmitType::class, array('label' => 'Search', 'attr' => ['class' => 'waves-effect waves-light btn']))
            ->getForm();
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }



}
