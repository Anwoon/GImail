<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     */
    public function index()
    {
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->findAll();

        return $this->render('contact/index.html.twig', [
            'contacts' => $contacts,
        ]);
    }

    /**
     * @Route("/contact/ajax", name="contact_ajax")
     */
    public function ajax()
    {
        $contacts = $this->getDoctrine()->getRepository(Contact::class)->findAll();
        $array_json = [];

        foreach ($contacts as $contact){
            $array_json[] = $contact->getMail();
        }

        return new JsonResponse($array_json);
    }

    /**
     * @Route("/contact/new", name="contact_new")
     */
    public function new(Request $request)
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact)
            ->add('save', SubmitType::class, ['label' => 'Créer', 'attr' => ['class' => 'waves-effect waves-light btn']]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            return $this->redirect('/contact');
        }
        return $this->render('contact/new.html.twig', [ 'form' => $form->createView(),'name'=>'Nouveau']);
    }

    /**
     * @Route("/contact/delete/{id}", name="contact_delete")
     */
    public function delete($id)
    {
        $contact = $this->getDoctrine()->getRepository(Contact::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();

        if (!$contact) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $entityManager->remove($contact);
        $entityManager->flush();

        return $this->redirectToRoute('contact');
    }

    /**
     * @ROute("/contact/update/{id}", name="contact_update")
     */
    public function update(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $contact = $em->getRepository(Contact::class)->find($id);
        $form = $this->createForm(ContactType::class, $contact)
            ->add('save', SubmitType::class, ['label' => 'Editer', 'attr' => ['class' => 'waves-effect waves-light btn']]);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $em->flush();

            return $this->redirect('/contact');
        }
        return $this->render('contact/new.html.twig', [ 'form' => $form->createView(),'name'=>'Edition']);
    }
}
