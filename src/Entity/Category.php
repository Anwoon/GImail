<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Email", mappedBy="category")
     */
    private $Email;

    public function __construct()
    {
        $this->Email = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Email[]
     */
    public function getEmail(): Collection
    {
        return $this->Email;
    }

    public function addEmail(Email $email): self
    {
        if (!$this->Email->contains($email)) {
            $this->Email[] = $email;
            $email->setCategory($this);
        }

        return $this;
    }

    public function removeEmail(Email $email): self
    {
        if ($this->Email->contains($email)) {
            $this->Email->removeElement($email);
            // set the owning side to null (unless already changed)
            if ($email->getCategory() === $this) {
                $email->setCategory(null);
            }
        }

        return $this;
    }
}
